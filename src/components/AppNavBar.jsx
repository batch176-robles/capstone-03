
import { Nav, Navbar, NavbarBrand, Offcanvas } from 'react-bootstrap';

const AppNavBar = () => {


    return (
        <Navbar variant='light' className='d-flex flex-row justify-content-between ps-3'>
            {/* Mobile Nav */}

            <Navbar.Toggle className='d-block d-md-none' aria-controls={`offcanvasNavbar-expand-${false}`} />
            <Navbar.Offcanvas
                id={`offcanvasNavbar-expand-${false}`}
                aria-labelledby={`offcanvasNavbarLabel-expand-${false}`}
                placement="start"
                className='canvass'
            >
                <Offcanvas.Header closeButton className='justify-content-start'>
                    <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${false}`}>
                    </Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body >
                    <Nav className="flex-grow-1 pe-3">
                        <Nav.Link id='navlink2' className='px-4 text-center'>Menus</Nav.Link>
                        <Nav.Link id='navlink2' className='px-4 text-center'>Stores</Nav.Link>
                        <Nav.Link id='navlink2' className='px-4 text-center'>About</Nav.Link>
                        <Nav.Link id='navlink2' className='px-4 text-center'>Contact</Nav.Link>
                    </Nav>
                </Offcanvas.Body>
            </Navbar.Offcanvas>

            <Nav>
                <NavbarBrand className='ms-3 d-none d-md-block'>WeekendBaker MNL</NavbarBrand>
            </Nav>





            <Nav className='d-none d-md-flex'>
                <Nav.Link id='navlink' className='px-4'>Menus</Nav.Link>
                <Nav.Link id='navlink' className='px-4'>Stores</Nav.Link>
                <Nav.Link id='navlink' className='px-4'>About</Nav.Link>
                <Nav.Link id='navlink' className='px-4'>Contact</Nav.Link>
            </Nav>
            <Nav className='me-3'>
                <Nav.Link className='navlogo'><img className="img-fluid navlogo2" alt="search" src={require('../images/search.png')} /></Nav.Link>
                <Nav.Link className='navlogo'><img className="img-fluid navlogo2" alt="user" src={require('../images/user.png')} /></Nav.Link>
                <Nav.Link className='navlogo'><img className="img-fluid navlogo2" alt="cart" src={require('../images/cart.png')} /></Nav.Link>
            </Nav>
        </Navbar >
    )
}

export default AppNavBar