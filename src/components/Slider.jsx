import React from 'react';
import { Carousel } from 'react-bootstrap';

const Slider = () => {
    return (
        /* MOBILE */
        <>
            <div className='d-block d-md-none justify-content-center'>
                < Carousel id="carousel p-5" >
                    <Carousel.Item interval={5000} className="p-5">
                        <img
                            className="d-block w-100 carouselimg"
                            src="https://images.pexels.com/photos/9316530/pexels-photo-9316530.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                            alt="First slide"
                        />
                        <Carousel.Caption>
                            <h3>First slide label</h3>
                            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item interval={5000} className="p-5">
                        <img
                            className="d-block w-100 carouselimg"
                            src="https://images.pexels.com/photos/1169790/pexels-photo-1169790.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                            alt="Second slide"
                        />
                        <Carousel.Caption>
                            <h3>Second slide label</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item interval={5000} className="p-5">
                        <img
                            className="d-block w-100 carouselimg"
                            src="https://images.pexels.com/photos/3602269/pexels-photo-3602269.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                            alt="Third slide"
                        />
                        <Carousel.Caption>
                            <h3>Third slide label</h3>
                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel >
            </div >

            <div className='d-none d-md-flex justify-content-center'>
                <Carousel id="carousel p-5">
                    <Carousel.Item interval={5000} className="p-5">
                        <img
                            className="d-block flex-grow-1 w-100 carouselimg"
                            src="https://images.pexels.com/photos/9316530/pexels-photo-9316530.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                            alt="First slide"
                        />
                        <Carousel.Caption>
                            <h3>First slide label</h3>
                            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item interval={5000} className="p-5">
                        <img
                            className="d-block flex-grow-1 w-100 carouselimg"
                            src="https://images.pexels.com/photos/1169790/pexels-photo-1169790.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                            alt="Second slide"
                        />
                        <Carousel.Caption>
                            <h3>Second slide label</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item interval={5000} className="p-5">
                        <img
                            className="d-block flex-grow-1 w-100 carouselimg"
                            src="https://images.pexels.com/photos/3602269/pexels-photo-3602269.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                            alt="Third slide"
                        />
                        <Carousel.Caption>
                            <h3>Third slide label</h3>
                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
            </div>
        </>
    )
}

export default Slider